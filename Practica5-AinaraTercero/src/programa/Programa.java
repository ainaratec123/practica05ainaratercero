package programa;

import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//creacion de instancia
		
		clases.GestorEntradas gestor = new clases.GestorEntradas();
		//creacion de escaner
		Scanner escaner = new Scanner(System.in);
		 int opcion;
		
		//creacion de menu
		do {
		System.out.println("***********MENU***************");
		System.out.println("1- Da de alta al Cliente");
		System.out.println("2- Lista los clientes");
		System.out.println("3- Buscar cliente");
		System.out.println("4- Eliminar Cliente");
		System.out.println("5- Dar de alta entrada");
		System.out.println("6- Eliminar Entrada");
		System.out.println("7- Buscar entrada");
		System.out.println("8- Listar entradas por precio");
		System.out.println("9- Listar entradas de un cliente");
		System.out.println("10- Asignar Cliente a entrada");
		System.out.println("11- Editar nombre Cliente");
		System.out.println("12- Editar codigo Entrada");
		System.out.println("13- Media del precio de las entradas");
		System.out.println("14- Precio maximo");
		System.out.println("15- Salir");
		System.out.println("Introduce una opcion:");
		opcion = escaner.nextInt();
		escaner.nextLine();
		switch (opcion) {
		
		
		case 1:
			//metodo 1 dar de alta a clientes
			System.out.println("Introduce el nombre del cliente");
			String nombre = escaner.nextLine();
			System.out.println("Introdue el apellido del cliente:");
			String apellido = escaner.nextLine();
			System.out.println("Intoduce el dni del cliente");
			String dni = escaner.nextLine();
			System.out.println("Introduce el telefono del cliente");
			
			int telefono = escaner.nextInt();
			
			gestor.altaCliente(nombre, apellido, dni, telefono);
			gestor.listarCliente();
		
			break;
		
		case 2:
			//metodo 2 listar clientes
			System.out.println("Has elegido la opcion de mostrar a los clientes");
			gestor.listarCliente();
		
			break;
		
		case 3:
			//metodo 3 buscar cliente
			System.out.println("Has elegido la opcion de buscar cliente");
			System.out.println("Intoduce el nombre del cliente que quieres buscar:");
			String nombreCliente = escaner.next();
			gestor.buscarCliente(nombreCliente);
		
			break;
		
		
		case 4:
			//metodo 4 eliminar cliente 
			System.out.println("Has selecionado la opcion de eliminar cliente");
			System.out.println("Intrduce el dni del cliente que quieres eliminar");
			String dniCliente = escaner.nextLine();
			gestor.eliminarCliente(dniCliente);
			gestor.listarCliente();
			
			
			break;
		
		
		case 5:
			
			//metodo 5 Dar de alta entrada
			
			System.out.println("Introduce el codigo de la entrada");
			String codEntrada = escaner.nextLine();
			System.out.println("Introduce la fecha de compra de la entrada");
			String fechaEntrada = escaner.nextLine();
			System.out.println("Introduce el precio de la entrada");
			double precio = escaner.nextDouble();
			escaner.nextLine();
			
			gestor.altaEntrada(codEntrada, fechaEntrada, precio);
			gestor.listarEntradas();
		break;
		
		
		case 6:
			//Metodo 6 eliminar entrada
			System.out.println("Has seleconado la opcion de elminar entrada");
			System.out.println("Intrduce el codigo de entrada para eliminarla");
			String codEntradaEliminada = escaner.nextLine();
			gestor.eliminaraEntrada(codEntradaEliminada);
			gestor.listarEntradas();
		break;
		
		case 7:
			//metodo 7 buscar entrada por codigo
			System.out.println("Has selecionado la opcion de buscar entrada por su codigo");
			System.out.println("Solo saldra uno debido que no habra entradas con el mismo codigo");
			System.out.println("Introduce el codigo de la entrada");
			
			String codEntradaBuscada = escaner.nextLine();
			gestor.busquedaEntrada(codEntradaBuscada);
		break;
		
	
		case 8:
			//metodo 8 listar entrada por precio
			System.out.println("Has selecionado la opcion de listar por precio");
			System.out.println("Introduce el precio de la entrada");
			double precioEntrada = escaner.nextDouble();
			gestor.listarEntradasPorPrecio(precioEntrada);
		break;
		
		case 9:
			//metodo 9 listar entradas de un clienta
			System.out.println("Has selecionado la opcion de listar entradas de un cliente");
			System.out.println("Introduce el dni del cliente que quieres miraar las entradas que tiene:");
			String dniEntrada = escaner.nextLine();
			gestor.listarClienteEntrada(dniEntrada);
			break;
		
		case 10:
			//metodo 10 asignar entrada a cliente
			System.out.println("Has selecionado la opcion de asignar entrada a cliente");
			System.out.println("Introduce el dni del cliente:");
			String dniClienteAsignado = escaner.nextLine();
			System.out.println("Introduce el codigo de la entrada:");
			String codEntradaAsignada = escaner.nextLine();
			gestor.asignarCliente(dniClienteAsignado, codEntradaAsignada);
		break;
		
		case 11:
			//metodo 11 Cambiar nombre a cliente
			System.out.println("Has selecionado la opcion de editar nombre de cliente");
			System.out.println("Introduce el nombre que quieres cambiar:");
			String nombreCambiar = escaner.nextLine();
			System.out.println("Introduce el nombre nuevo");
			String nombreNuevo = escaner.nextLine();
			gestor.cambiarNombreCliente(nombreCambiar, nombreNuevo);
			gestor.listarCliente();
		break;
		
		case 12:
			//metodo 12 cambiar codEntrada
			System.out.println("Has selecionado la opcion de editar codigo entrada");
			System.out.println("Introduce el codigo que quieres cambiar:");
			String codigoCambiar = escaner.nextLine();
			System.out.println("Introduce el codigo nuevo");
			String codigoNuevo = escaner.nextLine();
			gestor.cambiarCodigoEntrada(codigoCambiar, codigoNuevo);
			gestor.listarEntradas();
		break;
		
		case 13:
			//metodo 13 media de los precios
			System.out.println("Has selecionado la opcion de media del precio de las entradas");
			gestor.mediaEntrada();
		break;
		
		case 14:
			// metodo 14 entrada mas cara
			System.out.println("Has selecionado la opcion de la entrada mas cara");
			gestor.maxEntrada();
			
		break;
			
			
		case 15:
			//metodo 15 salir
			System.out.println("Has seleciona la opcion de salir ");
			System.exit(0);
			break;
		
		}
		
		
		//bucle del menu 
		}while(opcion!=15);
		
		//cerraos escaner
		escaner.close();	
		
		
	}

}
