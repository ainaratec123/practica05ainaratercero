package clases;

import java.time.LocalDate;

public class Entrada {
	//atributos de Entrada
	private String codEntrada;
	private LocalDate fechaConcierto;
	private LocalDate fechaCompra;
	private double precio;
	private Cliente clienteEntrada;
	

	//constructor
	public Entrada(String codEntrada) {
		this.codEntrada = codEntrada;
		
	}
	//getter y setter

	public String getCodEntrada() {
		return codEntrada;
	}
	public void setCodEntrada(String codEntrada) {
		this.codEntrada = codEntrada;
	}
	public LocalDate getFechaConcierto() {
		return fechaConcierto;
	}
	public void setFechaConcierto(LocalDate fechaConcierto) {
		this.fechaConcierto = fechaConcierto;
	}
	public LocalDate getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	
	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}

	
	public Cliente getClienteEntrada() {
		return clienteEntrada;
	}


	public void setClienteEntrada(Cliente cliente) {
		this.clienteEntrada = cliente;
	}

//to String
	@Override
	public String toString() {
		return "Entrada [codEntrada=" + codEntrada + ", fechaConcierto=" + fechaConcierto + ", fechaCompra="
				+ fechaCompra + ", precio=" + precio + ", clienteEntrada=" + clienteEntrada + "]";
	}
	


}
