package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorEntradas {
//creacion de Arrays 
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Entrada> listaEntradas;
	
	
	
	public GestorEntradas() {
		listaClientes = new ArrayList<Cliente>();
		listaEntradas = new ArrayList<Entrada>();
	}
	
	
	//metodo alta cliente
	 public void altaCliente (String nombre, String apellido, String dni, int telefono) {
			if (!existeCliente(dni)) {
				Cliente nuevoCliente = new Cliente (nombre, dni);
				nuevoCliente.setApellido(apellido);
				nuevoCliente.setTelefono(telefono);
				listaClientes.add(nuevoCliente);
			}
			
			else {
				System.out.println("El cliente ya existe");
			}
			 
		 }
		 
	 
	 //metodo si existe cantante
	 public boolean existeCliente(String dni) {
		 for (Cliente cliente : listaClientes) {
			 if (cliente != null & cliente.getDni().equals(dni)) {
				 return true;
			 }
		 }
		 
		 return false;
	 }
	 
	 
	 //metodo listar clientes
	 public void listarCliente() {
		 
		 
		 for (int i =0; i<listaClientes.size(); i++) {
			 Cliente cliente = listaClientes.get(i);
			 if (cliente != null) {
				 System.out.println(cliente);
			 }
		 } 
	 }
	 
	 //metodo buscar cliente
	 public Cliente buscarCliente(String dni) {
			for (Cliente cliente1 : listaClientes) {
				if (cliente1 != null && cliente1.getDni().equals(dni)) {
					return cliente1;
				}
			}
			return null;
		}
	 
	//metodo eliminar cliente
	public void eliminarCliente(String dni) {
		Iterator<Cliente> iteratorCliente = listaClientes.listIterator();
	
		
		while(iteratorCliente.hasNext()) {
			Cliente cliente = iteratorCliente.next();
			if (cliente.getDni().equals(dni)) {
				iteratorCliente.remove();
			}
		}
		
	}
	
	//metodo alta entrada
	public void altaEntrada(String codEntrada,String fechaCompra,  double precio) {
		
		if (!existeEntrada(codEntrada)) {
			Entrada nuevaEntrada = new Entrada(codEntrada);
			nuevaEntrada.setFechaConcierto(LocalDate.now());
			nuevaEntrada.setFechaCompra(LocalDate.parse(fechaCompra));
			nuevaEntrada.setPrecio(precio);
			listaEntradas.add(nuevaEntrada);
		}
		
	
		System.out.println("La entrada ya esta vendida");
	}
	

	//metodo si existe entrada
	public boolean existeEntrada(String codEtrada) {
		for (Entrada entrada :listaEntradas) {
			if (entrada != null && entrada.getCodEntrada().equals(codEtrada)) {
				return true;
			
				}
			}
		return false;
	}
	
	
	
	//metodo listar entradas
	public void listarEntradas() {
		
		for (int i =0; i<listaEntradas.size(); i++) {
			 Entrada entrada = listaEntradas.get(i);
			 if (entrada != null) {
				 System.out.println(entrada);
			 }
		 } 
		
	}
	
	//metodo eliminar entradas
	public void eliminaraEntrada(String codEntrada) {
		
		Iterator<Entrada> iteratorEntrada = listaEntradas.listIterator();
	
	while (iteratorEntrada.hasNext()) {
		Entrada entrada = iteratorEntrada.next();
			if (entrada.getCodEntrada().equals(codEntrada)) {
				iteratorEntrada.remove();
			}		
		}	
	}
	
	//metodo buscar entradass
	public Entrada busquedaEntrada(String codEntrada) {
	
		for (int i =0; i<listaEntradas.size(); i++) {
			Entrada entrada= listaEntradas.get(i);
			if ( entrada != null && entrada.getCodEntrada().equals(codEntrada)) {
				System.out.println(entrada);
				return entrada;
			}
		}
		System.out.println("La entrada buscada no existe");
		return null;
	}
	
//metodo listar entradas por precio
	public void listarEntradasPorPrecio(double precio) {
		
		for (Entrada entrada : listaEntradas) {
			if (entrada.getPrecio()==precio){
				System.out.println(entrada);
			}
		}
		
		
	}
	
	//metodo asignar entrada a cliente
	public void asignarCliente(String dni,  String codEntrada ) {
			
			if (buscarCliente(dni) != null && busquedaEntrada(codEntrada)!=null) {
				Cliente cliente = buscarCliente(dni);
				Entrada entrada = busquedaEntrada(codEntrada);
				entrada.setClienteEntrada(cliente);
			}
	
		}
	
	
	//metodo listar Cliententrada
	
	public void listarClienteEntrada(String dni) {
		for (Entrada entrada : listaEntradas) {
			if (entrada.getClienteEntrada()!=null && entrada.getClienteEntrada().getDni().equals(dni)) {
				System.out.println(entrada);
			}
		}	
	}
	
	//metodo cambiar nombre de cliente
	public void cambiarNombreCliente(String nombre, String nombre2) {
		for (int i = 0; i < listaClientes.size(); i++) {
			Cliente cliente = listaClientes.get(i);
			if (cliente != null) {
				if (cliente.getNombre().equals(nombre)) {
					cliente.setNombre(nombre2);
				}
			}
		}
	}
	
//metodo cambiar codigo entrada
	public void cambiarCodigoEntrada(String codEntrada, String codEntrada2) {
		for (int i = 0; i < listaEntradas.size(); i++) {
			Entrada entrada = listaEntradas.get(i);
			if (entrada != null) {
				if (entrada.getCodEntrada().equals(codEntrada)) {
					entrada.setCodEntrada(codEntrada2);
				}
			}
		}
	}
	
	
	//metodo media entrada
	public void mediaEntrada() {
		double total =0 ;
		for (Entrada entrada : listaEntradas) {
			if (entrada != null) {
				total = total + entrada.getPrecio();
			}
		}
		
		System.out.println(total/listaEntradas.size());
	}
	
	//metodo entrada as cara
	
	public void maxEntrada() {
		double max =0;
		for (Entrada entrada : listaEntradas) {
			if (entrada != null) {
				if(max<entrada.getPrecio()) {
					max = entrada.getPrecio();
				}
			}
			
		}
		System.out.println("El precio maximo es " + max);
		
	}
	
	
}
